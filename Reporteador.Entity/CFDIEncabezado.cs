﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class CFDIEncabezado
    {
        List<CFDIDetalle> Detalles;
        public int Anio { get; set; }
        public int IdEmpleado { get; set; }
        public string Nomina { get; set; }
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public int IdPeriodo { get; set; }
        public int ClaveMovimiento { get; set; }
        public string Version { get; set; }
        public string Serie { get; set; }
        public string Folio { get; set; }
        public string FechaElaboracion { get; set; }
        public string FormaPago { get; set; }
        public string CondicionesPago { get; set; }
        public string MetodoPago { get; set; }
        public string TipoComprobante { get; set; }
        public string LugarExpedicion { get; set; }
        public string NumeroCuenta { get; set; }
        public string FolioFiscal { get; set; }
        public string SerieFolioFiscal { get; set; }
        public string FechaFolioFiscal { get; set; }
        public Decimal MontoFolioFiscal { get; set; }
        
        public string EmisorCompania { get; set; }
        public string EmisorNombre { get; set; }
        public string EmisorCalle { get; set; }
        public string EmisorNoExterior { get; set; }
        public string EmisorNoInterior { get; set; }
        public string EmisorColonia { get; set; }
        public string EmisorLocalidad { get; set; }
        public string EmisorReferencia { get; set; }
        public string EmisorMunicipio { get; set; }
        public string EmisorEstado { get; set; }
        public string EmisorPais { get; set; }
        public string EmisorCodigoPostal { get; set; }
        public string EmisorTelefono { get; set; }

        public string ExpedidoEnCalle { get; set; }
        public string ExpedidoEnNoExterior { get; set; }
        public string ExpedidoEnNoInterior { get; set; }
        public string ExpedidoEnColonia { get; set; }
        public string ExpedidoEnLocalidad { get; set; }
        public string ExpedidoEnReferencia { get; set; }
        public string ExpedidoEnMunicipio { get; set; }
        public string ExpedidoEnEstado { get; set; }
        public string ExpedidoEnPais { get; set; }
        public string ExpedidoEnCodigoPostal { get; set; }
        public string EmisorDireccionCompleta { get; set; }

        public string ReceptorRfc { get; set; }
        public string ReceptorNombre { get; set; }
        public string ReceptorCalle { get; set; }
        public string ReceptorNoExterior { get; set; }
        public string ReceptorNoInterior { get; set; }
        public string ReceptorColonia { get; set; }
        public string ReceptorLocalidad { get; set; }
        public string ReceptorReferencia { get; set; }
        public string ReceptorMunicipio { get; set; }
        public string ReceptorEstado { get; set; }
        public string ReceptorPais { get; set; }
        public string ReceptorCodigoPostal { get; set; }
        public string ReceptorDireccionCompleta { get; set; }

        public Decimal SubTotal { get; set; }
        public Decimal Descuento { get; set; }
        public string MotivoDescuento { get; set; }
        public string Moneda { get; set; }
        public string TipoCambio { get; set; }
        public Decimal Total { get; set; }
        public string ImporteLetra { get; set; }
        public string RegimenFiscal { get; set; }
        public string Cantidad { get; set; }
        public string UnidadMedida { get; set; }
        public string NoIdentificacion { get; set; }
        public string Descripcion { get; set; }
        public Decimal ValorUnitario { get; set; }
        public Decimal Importe { get; set; }
        public string Impuestos { get; set; }
        public int Tasa { get; set; }
        public Decimal ImporteImpuesto { get; set; }
        public string RegistroPatronal { get; set; }
        public int NumEmpleado { get; set; }
        public string Curp { get; set; }
        public string TipoRegimen { get; set; }
        public string NumImss { get; set; }
        public string FechaPago { get; set; }
        public string FechaInicial { get; set; }
        public string FechaFinal { get; set; }
        public Decimal NumDias { get; set; }
        public string Departamento { get; set; }
        public string Clabe { get; set; }
        public string Banco { get; set; }
        public string FechaInicioRealLaboral { get; set; }
        public int Antiguedad { get; set; }
        public string Puesto { get; set; }
        public string TipoContrato { get; set; }
        public string TipoJornada { get; set; }
        public string PeriocidadPago { get; set; }
        public Decimal SalarioBase { get; set; }
        public string RiesgoPuesto { get; set; }
        public Decimal SalarioDiarioIntegrado { get; set; }
        public Decimal TotalGravado { get; set; }
        public Decimal TotalExento { get; set; }
        public Decimal TotalGravadoDeducciones { get; set; }
        public Decimal TotalExentoDeducciones { get; set; }
        public int Error { get; set; }
        public string Estatus { get; set; }
        public int IdCampo { get; set; }
        public int IdUsuario { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string CC { get; set; }
        public Decimal SueldoDiario { get; set; }
        public string Email { get; set; }
        public DateTime FechaAlta { get; set; }

        public decimal SueldoAdmvo { get; set; }
        public string CampoAdicional1 { get; set; }
        public string CampoAdicional2 { get; set; }
        public string CampoAdicional3 { get; set; }
        public DateTime FechaBaja { get; set; }
        public Decimal TipoSueldo { get; set; }
        public decimal NetoNomina{ get; set; }

        public string Subsidio { get; set; }
    }
}

