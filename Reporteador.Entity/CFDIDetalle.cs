﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class CFDIDetalle
    {
        public string RfcCompania { get; set; }
        public string RfcEmpleado { get; set; }
        public string Nomina { get; set; }
        public int IdEmpleado { get; set; }
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public int IdPeriodo { get; set; }
        public int ClaveMovimiento { get; set; }
        public string Tipo { get; set; }
        public string TipoPercepcion { get; set; }
        public string ClavePercepcion { get; set; }
        public string ConceptoPercepcion { get; set; }
        public Decimal ImporteGravado { get; set; }
        public Decimal ImporteExcento { get; set; }
        public int DiasIncapacidad { get; set; }
        public string TipoIncapacidad { get; set; }
        public Decimal Descuento { get; set; }
        public int DiasExtra { get; set; }
        public string TipoHorasExtra{ get; set; }
        public Decimal HorasExtra { get; set; }
        public Decimal ImportePagadoHorasExtra { get; set; }
        public int IdUsuario { get; set; }
        public Decimal Importe1 { get; set; }
        public Decimal Importe2 { get; set; }
        public Decimal Importe3 { get; set; }
        public Decimal SaldoActual { get; set; }
        public Decimal SaldoAnterior { get; set; }
        public Decimal Aplicado { get; set; }
        public int tipoSaldo { get; set; }
        public decimal TipoImporte1 { get; set; }
        #region MENSUAL
        public int CuentaPeriodo { get; set; }

        #endregion
    }
}
