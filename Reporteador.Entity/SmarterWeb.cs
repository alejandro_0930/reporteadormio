﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class SmarterWeb
    {
        public string Usuario { get; set; }
        public string Contraseña { get; set; }
        public string TokenSW { get; set; }
        public string Url { get; set; }
    }
}
