﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class CFDIRecibo
    {
        public int IdEmpleado { get; set; }
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public int IdPeriodo { get; set; }
        public int ClaveMovimiento { get; set; }
        public string CodigoError { get; set; }
        public string MensajeError { get; set; }
        public byte[] ArchivoXmlSellado { get; set; }
        public byte[] ArchivoXmlTimbrado { get; set; }
        public string CadenaOriginal { get; set; }
        public string Version { get; set; }
        public string UUID { get; set; }
        public DateTime FechaTimbre { get; set; }
        public string Sello { get; set; }
        public string Certificado { get; set; }
        public string CertificadoSat { get; set; }
        public string SelloSat { get; set; }
        public string FolioCancelacion { get; set; }
        public string EstatusFolioCancelacion { get; set; }
        public DateTime FechaCancelacion { get; set; }
        public DateTime FechaElaboracion { get; set; }
        public string NombreXML { get; set; }
        public string EmisorCompania { get; set; }
        

        public int EnvioMail { get; set; }
        public DateTime FechaEnvio { get; set; }
        public DateTime FechaReEnvio { get; set; }
        public string EmailReceptor { get; set; }
        public string EmailCC { get; set; }
        public string PAC { get; set; }
        public string UsuarioAcceso { get; set; }
        public string NumEmpleado { get; set; }

        #region MENSUAL
        public int Anio { get; set; }
        public int Mes { get; set; }
        #endregion

    }
}
