﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class Certificado
    {
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public int Global { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string CertificadoNombre { get; set; }
        public string Key { get; set; }
        public string Contrasena { get; set; }
        public string NoCertificado { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }

        public string NombrePfx { get; set; }

        public string NombrePem { get; set; }
    }
}

