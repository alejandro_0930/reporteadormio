﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class ParametrosCFDI
    {
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public int Tipo { get; set; }
        public string Valor { get; set; }
        public string Key { get; set; }
        public string Descripcion { get; set; }
    }
    public class TipoParametro
    {
        public int Tipo { get; set; }
        public string Descripcion { get; set; }
    }

    public class ParametrosCorreo 
    {
        public int IdCompania { get; set; }
        public string TextoTitulo { get; set; }
        public string TextoCuerpoCorreo { get; set; }
        public string TextoSaludo { get; set; }
        public string TextoAtentamente { get; set; }
    }
}
