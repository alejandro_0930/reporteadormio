﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class Usuario
    {
        public string UsuarioAcceso { get; set; }
        public string User { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaModificacion { get; set; }
        public int Grupo { get; set; }
        public string Descripcion { get; set; }
        public string Clave { get; set; }
        public string NombreComputadora { get; set; }
        public string NombreCompleto { get; set; }
        public string DireccionCorreo { get; set; }
        public int Administrador { get; set; }
        public int Acceso { get; set; }
        public int Validador { get; set; }
    }
}
