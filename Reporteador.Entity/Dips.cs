﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class Dips
    {
        public string Dip { get; set; }
        public string Descripcion { get; set; }
        public string DipDescripcion { get; set; }
        public int Totaliza { get; set; }
        public int Tipo_dip { get; set; }
        public int Impresion { get; set; }
    }
}
