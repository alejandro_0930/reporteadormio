﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class ReciboPTU
    {

        public int Anio { get; set; }
        public int IdEmpleado { get; set; }
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public int ClaveMovimiento { get; set; }
        public int EstatusEmpl { get; set; }

        public string ReceptorNombreCompleto { get; set; }
        public int ReceptorNoEmpleado { get; set; }
        public string IdUsuario { get; set; }
        public string Email { get; set; }

        public string Dip { get; set; }

    }
}
