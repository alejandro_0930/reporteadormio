﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Reporteador.Entity
{
     public static class SessionApp
    {
         public static string CadenaConexion
         {
             get
             {
                 string salida = "";
                 if (HttpContext.Current.Session["CadenaConexion"] != null)
                 {
                     salida = ((string)HttpContext.Current.Session["CadenaConexion"]);
                 }
                 return salida;
             }
             set
             {
                 if (value == null)
                 {
                     HttpContext.Current.Session.Remove("CadenaConexion");
                 }
                 else
                 {
                     HttpContext.Current.Session["CadenaConexion"] = value;
                 }
             }
         }
    }
}
