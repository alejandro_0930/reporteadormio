﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class CatalogoSat
    {
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public string IdDetalleNomina { get; set; }
        public string ClaveSat { get; set; }
        public string Descripcion { get; set; }
        public string ClaveNomina { get; set; }
    }
}
