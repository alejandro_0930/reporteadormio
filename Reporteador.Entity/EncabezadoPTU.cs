﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
   public class EncabezadoPTU
    {
        public int Anio { get; set; }
        public int IdEmpleado { get; set; }        
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public int ClaveMovimiento { get; set; }  
         
        public string EmisorCompania { get; set; }
        public string EmisorNombre { get; set; }
        public string EmisorRegimenFiscal { get; set; }
        public string EmisorRegistroPatronal { get; set; }              
        public string EmisorDireccionCompleta { get; set; }
        public string EmisorCodigoPostal { get; set; }
        
        public string ReceptorPaterno { get; set; }
        public string ReceptorMaterno { get; set; }
        public string ReceptorNombre { get; set; }
        public int ReceptorNoEmpleado { get; set; }
        public string ReceptorRFC { get; set; }
        public string ReceptorCurp { get; set; }
        public string ReceptorNSS { get; set; }
        public string ReceptorPuesto { get; set; }
        public string ReceptorDepartamento { get; set; }
        public string ReceptorNombreCompleto { get; set; }


        public int Error { get; set; }
        public string IdUsuario { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string Email { get; set; }      
       
    }
}
