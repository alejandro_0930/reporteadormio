﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Reporteador.Entity
{
    public class DiverzaConnection
    {
        public Credentials credentials { get; set; }
        public Issuer issuer { get; set; }
        public Receiver receiver { get; set; }
        public BodyDocument document { get; set; }
    }

    public class Credentials
    {
        public string id { get; set; }
        public string token { get; set; }
    }

    public class Issuer
    {
        public string rfc { get; set; }
    }

    public class Email
    {
        public string email { get; set; }
        public string format { get; set; }
        public string template { get; set; }
    }

    public class Receiver
    {
        public List<Email> emails { get; set; }
        public Receiver()
        {
            emails = new List<Email>();
        }
    }

    public class BodyDocument
    {
        [JsonProperty(PropertyName = "ref-id")]
        public string ref_id { get; set; }
        [JsonProperty(PropertyName = "certificate-number")]
        public string certificate_number { get; set; }
        public string section { get; set; }
        public string format { get; set; }
        public string template { get; set; }
        public string type { get; set; }
        public string content { get; set; }
    }

    public class Response
    {
        public string uuid { get; set; }
        [JsonProperty(PropertyName = "ref-id")]
        public string ref_id { get; set; }
        public string content { get; set; }
    }

}
