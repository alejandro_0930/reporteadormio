﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{

    public class Empleado
    {
        public int IdEmpleado { get; set; }
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public int IdPeriodo { get; set; }
        public int ClaveMovimiento { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Nombre { get; set; }
        public int NoEmpleado { get; set; }
        public string CompaniaDescripcion { get; set; }
        public string NominaDescripcion { get; set; }
        public string NombreCompleto { get; set; }
        public string Email { get; set; }
        public DateTime FechaInicial { get; set; }
        public DateTime FechaFinal { get; set; }


        #region MENSUAL

        public int Anio { get; set; }
        public int Mes { get; set; }
        public string Fecha { get; set; }

        #endregion
        public CFDIEncabezado CFDIEncabezado { get; set; }
        public List<CFDIDetalle> CFDIDetalle { get; set; }
    }
}
