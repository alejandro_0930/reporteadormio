﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class Periodo
    {
        public int Compania { get; set; }
        public int Nomina { get; set; }
        public int IdPeriodo { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaModificacion { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public int IdBimestre { get; set; }
        public int Anio { get; set; }
        public int Mes { get; set; }
        public int ConsecNomina { get; set; }
        public int PeriodosMes { get; set; }
        public int PeriodoEnMes { get; set; }
        public DateTime FechaDesdeImp { get; set; }
        public DateTime FechaHastaImp { get; set; }
        public int DiasImss { get; set; }
        public DateTime FechaFinMes { get; set; }
        public int PeriodoFondoAhorro { get; set; }
        public int IdPeriodoImss { get; set; }
        public float DiasPago { get; set; }
        //Esta descripcion se utiliza para pintarla en los combos de busqueda
        public string Descripcion { get; set; }
        public int idClaveMovimiento { get; set; }
    }
}
