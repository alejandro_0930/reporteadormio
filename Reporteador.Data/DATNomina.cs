﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATNomina
    {
        #region " Constantes "
        const string NOMBRE_APLICATIVO = "CFDI";
        const string NOMBRE_ENSAMBLADO = "AES.CFDI.Data";
        const string NOMBRE_CLASE = "DATNomina";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;

         public DATNomina()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
        }
        
        /// <summary>
        /// Obtiene Todas las nominas de una compañia
        /// </summary>
        /// <param name="sUsuario">Esto es Usuario logeado.</param> 
        /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        public List<Entity.Nomina> ObtenNomina(int iIdCompania, string sUsuarioAcceso)
        {
            string storedProcName = "CFDI_OBTEN_NOMINAS";
            Entity.Nomina eNomina;
            List<Entity.Nomina> lstNominas = new List<Nomina>();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "ID_COMPANIA", DbType.Int32, iIdCompania);
                    namedDB.AddInParameter(sprocCmd, "ID_NOMINA", DbType.Int32, 0);
                    namedDB.AddInParameter(sprocCmd, "USUARIO_ACCESO", DbType.String, sUsuarioAcceso);

                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            eNomina = new Entity.Nomina();
                            //eNomina.ClaveCarga = Functions.CheckStr(reader["CLAVE_CARGA"]);
                            eNomina.Descripcion = reader["DESCRIPCION"].ToString();
                            //eNomina.DiasDelPeriodo = Functions.CheckFloat(reader["DIAS_DEL_PERIODO"]);
                            //eNomina.DipProporcion = Functions.CheckStr(reader["DIP_PROPORCION"]);
                            //eNomina.DipSueldo = Functions.CheckStr(reader["DIP_SUELDO"]);
                            //eNomina.FechaModificacion = Functions.CheckDate(reader["FECHA_MODIFICACION"]);
                            //eNomina.FechaRegistro = Functions.CheckDate(reader["FECHA_REGISTRO"]);
                            eNomina.IdCompania = Int32.Parse(reader["COMPANIA"].ToString());
                            eNomina.IdNomina = Int32.Parse(reader["NOMINA"].ToString());
                            //eNomina.ProporcionSeptimo = Functions.CheckFloat(reader["PROPORCION_SEPTIMO"]);
                            //eNomina.ProporcionSueldo = Functions.CheckFloat(reader["PROPORCION_SUELDO"]);
                            //eNomina.Usuario = Functions.CheckStr(reader["USUARIO"]);
                            lstNominas.Add(eNomina);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenNomina: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return lstNominas;
        }
        
        /// <summary>
        /// Obtiene 1 nomina de una compañia
        /// </summary>
        /// <param name="sUsuario">Esto es Usuario logeado.</param> 
        /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        /// /// <param name="iIdNomina">Id de la nomina que se desea obtener.</param> 
        public Entity.Nomina ObtenNomina(int iIdCompania,int iIdNomina, string sUsuarioAcceso)
        {
            string storedProcName = "CFDI_OBTEN_NOMINAS";
            Entity.Nomina eNomina = null;
            
            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "ID_COMPANIA", DbType.Int32, iIdCompania);
                    namedDB.AddInParameter(sprocCmd, "ID_NOMINA", DbType.Int32, iIdNomina);
                    namedDB.AddInParameter(sprocCmd, "USUARIO_ACCESO", DbType.String, sUsuarioAcceso);

                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            eNomina = new Entity.Nomina();
                            eNomina.ClaveCarga = reader["CLAVE_CARGA"].ToString();
                            eNomina.Descripcion = reader["DESCRIPCION"].ToString();
                            eNomina.DiasDelPeriodo = float.Parse(reader["DIAS_DEL_PERIODO"].ToString());
                            eNomina.DipProporcion = reader["DIP_PROPORCION"].ToString();
                            eNomina.DipSueldo = reader["DIP_SUELDO"].ToString();
                            eNomina.FechaModificacion = DateTime.Parse(reader["FECHA_MODIFICACION"].ToString());
                            eNomina.FechaRegistro = DateTime.Parse(reader["FECHA_REGISTRO"].ToString());
                            eNomina.IdCompania = Int32.Parse(reader["COMPANIA"].ToString());
                            eNomina.IdNomina = Int32.Parse(reader["NOMINA"].ToString());
                            eNomina.ProporcionSeptimo = float.Parse(reader["PROPORCION_SEPTIMO"].ToString());
                            eNomina.ProporcionSueldo = float.Parse(reader["PROPORCION_SUELDO"].ToString());
                            eNomina.Usuario = reader["USUARIO"].ToString();                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenNomina: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return eNomina;
        }
    }
}
