﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATDips
    {
        #region " Constantes "
        const string NOMBRE_APLICATIVO = "Reporteador";
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data";
        const string NOMBRE_CLASE = "DATDips";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;

        public DATDips()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
        }

        /// <summary>
        /// Obtiene Todas las claves de movimiento
        /// </summary>        
        public List<Entity.Dips> ObtenDips()
        {
            string storedProcName = "Reporteador_OBTEN_DIPS";
            Entity.Dips eDips;
            List<Entity.Dips> lstDips = new List<Dips>();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            eDips = new Entity.Dips();
                            eDips.Dip = reader["DIP"].ToString();
                            eDips.Descripcion = reader["DESCRIPCION"].ToString();
                            eDips.DipDescripcion = reader["DIPDESCRIPCION"].ToString();
                            eDips.Totaliza = Int32.Parse(reader["TOTALIZA"].ToString());
                            eDips.Tipo_dip = Int32.Parse(reader["TIPO_DIP"].ToString());
                            eDips.Impresion = Int32.Parse(reader["IMPRESION"].ToString());
                            lstDips.Add(eDips);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenDips: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return lstDips;
        }

    }
}