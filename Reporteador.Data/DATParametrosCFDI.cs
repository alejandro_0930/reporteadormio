﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATParametrosCFDI
    {
        #region " Constantes "
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data.";
        const string NOMBRE_CLASE = "DATParametrosCFDI.";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;
        public DATParametrosCFDI()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
        }

        public string ObtenParametroNomina(string sGrupo, string sVariable)
        {
            List<Entity.ParametrosCFDI> lstParametros = new List<ParametrosCFDI>();
            string storedProcName = "CFDI_OBTEN_PARAMETRO_NOMINA";
            string sValor = "";
            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "GRUPO", DbType.String, sGrupo);
                    namedDB.AddInParameter(sprocCmd, "VARIABLE", DbType.String, sVariable);

                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            sValor = reader["valor"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenParametroNomina: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return sValor;
        }

        public string ObtenParametroNomina(string sGrupo, string sVariable,int iCompania, int iNomina)
        {
            List<Entity.ParametrosCFDI> lstParametros = new List<ParametrosCFDI>();
            string storedProcName = "CFDI_OBTEN_PARAMETRO_NOMINACIA";
            string sValor = "";
            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "GRUPO", DbType.String, sGrupo);
                    namedDB.AddInParameter(sprocCmd, "VARIABLE", DbType.String, sVariable);
                    namedDB.AddInParameter(sprocCmd, "COMPANIA", DbType.Int16, iCompania);
                    namedDB.AddInParameter(sprocCmd, "NOMINA", DbType.Int16, iNomina);

                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            sValor = reader["valor"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenParametroNomina: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return sValor;
        }

        public List<Entity.ParametrosCFDI> ObtenParametros(int iTipo, string sKey)
        {
            List<Entity.ParametrosCFDI> lstParametros = new List<ParametrosCFDI>();
            string storedProcName = "CFDI_OBTEN_PARAMETROS";
            Entity.ParametrosCFDI eParametros;

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "tipo", DbType.Int32, iTipo);
                    namedDB.AddInParameter(sprocCmd, "key", DbType.String, sKey);

                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            eParametros = new Entity.ParametrosCFDI();
                            eParametros.Descripcion =reader["descripcion"].ToString();
                            eParametros.FechaCreacion = DateTime.Parse(reader["fecha_creacion"].ToString());
                            eParametros.FechaModificacion = DateTime.Parse(reader["fecha_modificacion"].ToString());
                            eParametros.Tipo = Int32.Parse(reader["tipo"].ToString());
                            eParametros.Usuario = reader["usuario"].ToString();
                            eParametros.Valor = reader["valor"].ToString();
                            eParametros.Key = reader["key"].ToString();
                            lstParametros.Add(eParametros);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenParametros: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return lstParametros;
        }

    }
}
