﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Data
{
    public class DATAutos
    {
        #region " Constantes "
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data.";
        const string NOMBRE_CLASE = "DATAutos.";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;

        public DATAutos()
        {
            if (Entity.SessionApp.CadenaConexion != "")
                namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
            else
                namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
        }

        public void InsertaAutosExcesosBaan(string strUsuario, string strProcess, string strPeriod, string strDocument, string strNumber, string strDate, string strTaxPeriod, string strReportingPeriod, string strReference, string strCur, string strAmountFC, string strAmountDebit, string strAmountCredit)
        {
            string storedProcName = "Reporteador_INS_AUTOS_BAAN";
            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "usuario", DbType.String, strUsuario);
                    namedDB.AddInParameter(sprocCmd, "Proceso", DbType.String, strProcess);
                    namedDB.AddInParameter(sprocCmd, "Periodo", DbType.String, strPeriod);
                    namedDB.AddInParameter(sprocCmd, "Documento", DbType.String, strDocument);
                    namedDB.AddInParameter(sprocCmd, "Empleado", DbType.String, strNumber);
                    namedDB.AddInParameter(sprocCmd, "Fecha", DbType.String, strDate);
                    namedDB.AddInParameter(sprocCmd, "Periodo_Tax", DbType.String, strTaxPeriod);
                    namedDB.AddInParameter(sprocCmd, "Periodo_Reporte", DbType.String, strReportingPeriod);
                    namedDB.AddInParameter(sprocCmd, "Referencia", DbType.String, strReference);
                    namedDB.AddInParameter(sprocCmd, "Cantidad", DbType.String, strCur);
                    namedDB.AddInParameter(sprocCmd, "Cantidad_FC", DbType.String, strAmountFC);
                    namedDB.AddInParameter(sprocCmd, "Debito", DbType.String, strAmountDebit);
                    namedDB.AddInParameter(sprocCmd, "Credito", DbType.String, strAmountCredit);

                    namedDB.ExecuteNonQuery(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD InsertaAutosExcesosBaan: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
        }

        public void BorraAutosExcesosBaan(string strUsuario)
        {
            string storedProcName = "Reporteador_DEL_AUTOS_BAAN";
            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "usuario", DbType.String, strUsuario);

                    namedDB.ExecuteNonQuery(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD BorraAutosExcesosBaan: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
        }

    }
}
