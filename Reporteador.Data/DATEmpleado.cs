﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;
using System.IO;

namespace Reporteador.Data
{
    public class DATEmpleado
    {
        #region " Constantes "
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data.";
        const string NOMBRE_CLASE = "DATEmpleado.";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;
        string sActivaLog = "";

        public DATEmpleado()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
            Data.DATParametrosCFDI dParametro = new Data.DATParametrosCFDI();
            sActivaLog = dParametro.ObtenParametroNomina("RECIBOS XML", "ACTIVA_LOG_ERRORES", 0, 0);
        }

        /// <summary>
        /// Regresa lista de empleados que se mostrarán en el Grid
        /// </summary>        
        /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        /// <param name="iIdNomina">Id de la nomina que se desea obtener.</param> 
        /// <param name="iIdPeriodo">Id del periodo que se desea obtener.</param> 
        /// <param name="iClaveMovimiento">Id de la CLave Movimiento que se desea obtener.</param> 
        /// <param name="iIdEmpleado">Id del empleado que se desea obtener.</param> 
        /// <param name="iEstatus"> 0 = Todos     1 = Pendientes de envio.</param> 
        public List<Entity.Empleado> ObtenEmpleado(int iIdCompania, int iIdNomina, int iIdPeriodo, int iClaveMovimiento, int iNoEmpleado, int iEstatus, int iConEmail, int iOrden, string sUsuario)
        {
            string storedProcName = "CFDI_OBTEN_EMPLEADOS_GRID";
            Entity.Empleado eEmpleado;
            List<Entity.Empleado> lstEmpleados = new List<Empleado>();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "id_compania", DbType.Int32, iIdCompania);
                    namedDB.AddInParameter(sprocCmd, "id_nomina", DbType.Int32, iIdNomina);
                    namedDB.AddInParameter(sprocCmd, "id_periodo", DbType.Int32, iIdPeriodo);
                    namedDB.AddInParameter(sprocCmd, "empleado", DbType.Int32, iNoEmpleado);
                    namedDB.AddInParameter(sprocCmd, "id_clave_movimiento", DbType.Int32, iClaveMovimiento);
                    namedDB.AddInParameter(sprocCmd, "estatus", DbType.Int32, iEstatus);
                    namedDB.AddInParameter(sprocCmd, "sin_mail", DbType.Int32, iConEmail);
                    namedDB.AddInParameter(sprocCmd, "orden", DbType.Int32, iOrden);
                    namedDB.AddInParameter(sprocCmd, "usuario", DbType.String, sUsuario);

                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            eEmpleado = new Entity.Empleado();

                            eEmpleado.NoEmpleado = Int32.Parse(reader["empleado"].ToString());
                            eEmpleado.IdCompania = Int32.Parse(reader["compania"].ToString());
                            eEmpleado.IdEmpleado = Int32.Parse(reader["id"].ToString());
                            eEmpleado.IdNomina = Int32.Parse(reader["nomina"].ToString());
                            eEmpleado.Nombre = reader["nombre"].ToString();
                            eEmpleado.IdPeriodo = Int32.Parse(reader["periodo"].ToString());
                            eEmpleado.ClaveMovimiento = Int32.Parse(reader["clave_movimiento"].ToString());
                            eEmpleado.Email = reader["email"].ToString();
                            lstEmpleados.Add(eEmpleado);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenEmpleado: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return lstEmpleados;
        }

     }
}
