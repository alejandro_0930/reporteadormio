﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATReportes
    {
         #region " Constantes "
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data.";
        const string NOMBRE_CLASE = "DATReportes.";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;

        public DATReportes()
        {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
        }
        


        public DataSet RptTotalesCFDI(int iAnio, int iCompania, int iNomina, int iClave, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            string storedProcName = "Reporteador_CFDI_Totales";
            DataSet ds = new DataSet();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    sprocCmd.CommandTimeout = 0;
                    namedDB.AddInParameter(sprocCmd, "anio", DbType.Int32, iAnio);
                    namedDB.AddInParameter(sprocCmd, "compania", DbType.Int32, iCompania);
                    namedDB.AddInParameter(sprocCmd, "nomina", DbType.Int32, iNomina);
                    namedDB.AddInParameter(sprocCmd, "clave", DbType.Int32, iClave);
                    namedDB.AddInParameter(sprocCmd, "fecha_desde", DbType.String, dFechaDesde.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "fecha_hasta", DbType.String, dFechaHasta.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "filtro", DbType.String, sFiltroPeriodo);

                    ds = namedDB.ExecuteDataSet(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD RptTotalesCFDI: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return ds;
        }

        public DataSet RptTotalesCiaCFDI(int iAnio, int iCompania, int iNomina, int iClave, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            string storedProcName = "Reporteador_CFDI_Totales_cia";
            DataSet ds = new DataSet();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    sprocCmd.CommandTimeout = 0;
                    namedDB.AddInParameter(sprocCmd, "anio", DbType.Int32, iAnio);
                    namedDB.AddInParameter(sprocCmd, "compania", DbType.Int32, iCompania);
                    namedDB.AddInParameter(sprocCmd, "nomina", DbType.Int32, iNomina);
                    namedDB.AddInParameter(sprocCmd, "clave", DbType.Int32, iClave);
                    namedDB.AddInParameter(sprocCmd, "fecha_desde", DbType.String, dFechaDesde.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "fecha_hasta", DbType.String, dFechaHasta.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "filtro", DbType.String, sFiltroPeriodo);

                    ds = namedDB.ExecuteDataSet(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD RptTotalesCiaCFDI: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return ds;
        }

        public DataSet RptDetalleErroresCFDI(int iAnio, int iCompania, int iNomina, int iClave, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            string storedProcName = "Reporteador_CFDI_Detalle_Errores";
            DataSet ds = new DataSet();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    sprocCmd.CommandTimeout = 0;
                    namedDB.AddInParameter(sprocCmd, "anio", DbType.Int32, iAnio);
                    namedDB.AddInParameter(sprocCmd, "compania", DbType.Int32, iCompania);
                    namedDB.AddInParameter(sprocCmd, "nomina", DbType.Int32, iNomina);
                    namedDB.AddInParameter(sprocCmd, "clave", DbType.Int32, iClave);
                    namedDB.AddInParameter(sprocCmd, "fecha_desde", DbType.String, dFechaDesde.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "fecha_hasta", DbType.String, dFechaHasta.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "filtro", DbType.String, sFiltroPeriodo);

                    ds = namedDB.ExecuteDataSet(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD RptDetalleErroresCFDI: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return ds;
        }

        public DataSet RptDetalleCancelaCFDI(int iAnio, int iCompania, int iNomina, int iClave, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            string storedProcName = "Reporteador_CFDI_Detalle_Cancela";
            DataSet ds = new DataSet();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    sprocCmd.CommandTimeout = 0;
                    namedDB.AddInParameter(sprocCmd, "anio", DbType.Int32, iAnio);
                    namedDB.AddInParameter(sprocCmd, "compania", DbType.Int32, iCompania);
                    namedDB.AddInParameter(sprocCmd, "nomina", DbType.Int32, iNomina);
                    namedDB.AddInParameter(sprocCmd, "clave", DbType.Int32, iClave);
                    namedDB.AddInParameter(sprocCmd, "fecha_desde", DbType.String, dFechaDesde.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "fecha_hasta", DbType.String, dFechaHasta.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "filtro", DbType.String, sFiltroPeriodo);

                    ds = namedDB.ExecuteDataSet(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD RptDetalleCancelaCFDI: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return ds;
        }

        public DataSet RptDetalleValidadosCFDI(int iAnio, int iCompania, int iNomina, int iClave, string sStatus, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            string storedProcName = "Reporteador_CFDI_Detalle_Validados";
            DataSet ds = new DataSet();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    sprocCmd.CommandTimeout = 0;
                    namedDB.AddInParameter(sprocCmd, "anio", DbType.Int32, iAnio);
                    namedDB.AddInParameter(sprocCmd, "compania", DbType.Int32, iCompania);
                    namedDB.AddInParameter(sprocCmd, "nomina", DbType.Int32, iNomina);
                    namedDB.AddInParameter(sprocCmd, "clave", DbType.Int32, iClave);
                    namedDB.AddInParameter(sprocCmd, "estatus", DbType.String, sStatus);
                    namedDB.AddInParameter(sprocCmd, "fecha_desde", DbType.String, dFechaDesde.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "fecha_hasta", DbType.String, dFechaHasta.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "filtro", DbType.String, sFiltroPeriodo);

                    ds = namedDB.ExecuteDataSet(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD Reporteador_CFDI_Detalle_Validados: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return ds;
        }

        public DataSet RptDetalleAcumuladosCFDI(int iAnio, int iCompania, int iNomina, int iClave, string sStatus, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            string storedProcName = "Reporteador_CFDI_Detalle_Acumulados";
            DataSet ds = new DataSet();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    sprocCmd.CommandTimeout = 0;
                    namedDB.AddInParameter(sprocCmd, "anio", DbType.Int32, iAnio);
                    namedDB.AddInParameter(sprocCmd, "compania", DbType.Int32, iCompania);
                    namedDB.AddInParameter(sprocCmd, "nomina", DbType.Int32, iNomina);
                    namedDB.AddInParameter(sprocCmd, "clave", DbType.Int32, iClave);
                    namedDB.AddInParameter(sprocCmd, "estatus", DbType.String, sStatus);
                    namedDB.AddInParameter(sprocCmd, "fecha_desde", DbType.String, dFechaDesde.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "fecha_hasta", DbType.String, dFechaHasta.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "filtro", DbType.String, sFiltroPeriodo);

                    ds = namedDB.ExecuteDataSet(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD RptDetalleAcumuladosCFDI: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return ds;
        }

        public DataSet RptDetalleRecuperacionesCFDI(int iAnio, int iCompania, int iNomina, int iClave, string sStatus, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            string storedProcName = "Reporteador_CFDI_Detalle_Recupera";
            DataSet ds = new DataSet();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    sprocCmd.CommandTimeout = 0;
                    namedDB.AddInParameter(sprocCmd, "anio", DbType.Int32, iAnio);
                    namedDB.AddInParameter(sprocCmd, "compania", DbType.Int32, iCompania);
                    namedDB.AddInParameter(sprocCmd, "nomina", DbType.Int32, iNomina);
                    namedDB.AddInParameter(sprocCmd, "clave", DbType.Int32, iClave);
                    namedDB.AddInParameter(sprocCmd, "fecha_desde", DbType.String, dFechaDesde.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "fecha_hasta", DbType.String, dFechaHasta.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "filtro", DbType.String, sFiltroPeriodo);

                    ds = namedDB.ExecuteDataSet(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD RptDetalleRecuperacionesCFDI: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return ds;
        }

        public DataSet RptDetalleExpatriadosCFDI(int iAnio, int iCompania, int iNomina, int iClave, string sStatus, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            string storedProcName = "Reporteador_CFDI_Detalle_Timbrados";
            DataSet ds = new DataSet();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    sprocCmd.CommandTimeout = 0;
                    namedDB.AddInParameter(sprocCmd, "anio", DbType.Int32, iAnio);
                    namedDB.AddInParameter(sprocCmd, "compania", DbType.Int32, iCompania);
                    namedDB.AddInParameter(sprocCmd, "nomina", DbType.Int32, iNomina);
                    namedDB.AddInParameter(sprocCmd, "clave", DbType.Int32, iClave);
                    namedDB.AddInParameter(sprocCmd, "fecha_desde", DbType.String, dFechaDesde.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "fecha_hasta", DbType.String, dFechaHasta.ToString("yyyy/MM/dd"));
                    namedDB.AddInParameter(sprocCmd, "filtro", DbType.String, sFiltroPeriodo);

                    ds = namedDB.ExecuteDataSet(sprocCmd);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error BD RptDetalleExpatriadosCFDI: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return ds;
        }

    }
}
