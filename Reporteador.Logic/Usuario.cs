﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using Reporteador.Data;

namespace Reporteador.Logic
{
    public static class Usuario
    {   
        public static string CadenaConexion { get; set; }
        public static Entity.Usuario ValidaUsuario(string sUsuario, string sClave)
        {
            Entity.Usuario eUsuario = null;
            Data.DATUsuario.CadenaConexion = CadenaConexion;            
            Data.DATUsuario dUsuario = new Data.DATUsuario();

            eUsuario = dUsuario.ValidaUsuario(sUsuario, sClave);
            return eUsuario;
        }

        public static Entity.Usuario ValidaUsuarioSE(string sUsuario)
        {
            Entity.Usuario eUsuario = null;
            Data.DATUsuario dUsuario = new Data.DATUsuario();
            eUsuario = dUsuario.ValidaUsuarioSE(sUsuario);
            return eUsuario;
        }
    }
}
