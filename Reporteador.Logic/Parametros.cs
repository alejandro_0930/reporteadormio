﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Data;
using Reporteador.Entity;

namespace Reporteador.Logic
{
    public class Parametros
    {
        public static bool isLADPActive(string sGrupo, string sVariable)
        {
            DATParametro parametro = new DATParametro();
            return parametro.isLADPActive(sGrupo, sVariable);
        }
    }
}
