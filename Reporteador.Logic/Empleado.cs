﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Data;
using System.Data;
using Reporteador.Entity;

namespace Reporteador.Logic
{
    public static class Empleado
    {
        public static string CadenaConexion { get; set; }
        /// <summary>
        /// Regresa lista de empleados que se mostrarán en el Grid
        /// </summary>        
        /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        /// <param name="iIdNomina">Id de la nomina que se desea obtener.</param> 
        /// <param name="iIdPeriodo">Id del periodo que se desea obtener.</param> 
        /// <param name="iClaveMovimiento">Id de la CLave Movimiento que se desea obtener.</param> 
        /// <param name="iIdEmpleado">Id del empleado que se desea obtener.</param> 
        /// <param name="iEstatus"> 0 = Todos     1 = Pendientes de envio.</param> 
        public static List<Entity.Empleado> ObtenEmpleado(int iIdCompania, int iIdNomina, int iIdPeriodo, int iClaveMovimiento, int iNoEmpleado, int iEstatus, int iConMail, int iOrden, string sUsuario)
        {
            List<Entity.Empleado> lstEmpleados;
            DATEmpleado dEmpleado = new DATEmpleado();
            lstEmpleados = dEmpleado.ObtenEmpleado(iIdCompania, iIdNomina, iIdPeriodo, iClaveMovimiento, iNoEmpleado, iEstatus, iConMail, iOrden, sUsuario);
            return lstEmpleados;
        }

    }
}
