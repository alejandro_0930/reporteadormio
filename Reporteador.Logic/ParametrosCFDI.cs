﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Logic
{
    public static class ParametrosCFDI
    {
        public static string CadenaConexion { get; set; }
        public static List<Entity.ParametrosCFDI> ObtenParametros(int iTipo, string sKey)
        {
            Data.DATParametrosCFDI dParametro = new Data.DATParametrosCFDI();
            List<Entity.ParametrosCFDI> lstParametros;
            lstParametros = dParametro.ObtenParametros(iTipo, sKey);
            return lstParametros;
        }

        public static string ObtenParametroNomina(string sGrupo, string sVariable)
        {
            Data.DATParametrosCFDI dParametro = new Data.DATParametrosCFDI();
            string sValor = dParametro.ObtenParametroNomina(sGrupo, sVariable);
            return sValor;
        }
        public static string ObtenParametroNomina(string sGrupo, string sVariable,int ICompania, int INomina)
        {
            Data.DATParametrosCFDI dParametro = new Data.DATParametrosCFDI();
            string sValor = dParametro.ObtenParametroNomina(sGrupo, sVariable, ICompania, INomina);
            return sValor;
        }

    }
}
