﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Configuration;
using System.Net.Mail;
using System.Threading;
using System.IO;
using System.Runtime.CompilerServices;
using System.Web.UI.WebControls;
using Reporteador.Entity;
using System.Globalization;

namespace Reporteador.Logic
{

    public static class Utilities
    {
        public static void ManejaError(Exception obExcepcion, HttpResponse oResponse, string pPaginaRetorno)
        {

            Sessions.MensajeError = obExcepcion.Message;
            Sessions.StackTrace = obExcepcion.StackTrace;

            if (String.IsNullOrEmpty(pPaginaRetorno))
            {
                Sessions.URLRetorno = Constants.C_PAGINA_DEFAULT.ToString();
            }
            else
            {
                Sessions.URLRetorno = pPaginaRetorno;
            }

            oResponse.Redirect(Constants.C_PAGINA_ERROR.ToString(), false);
        }

        public static void ManejaMensaje(string istrMensaje, HttpResponse oResponse, string pPaginaRetorno)
        {

            Sessions.Mensaje = istrMensaje;

            if (String.IsNullOrEmpty(pPaginaRetorno))
            {
                Sessions.URLRetorno = Constants.C_PAGINA_DEFAULT.ToString();
            }
            else
            {
                Sessions.URLRetorno = pPaginaRetorno;
            }

            oResponse.Redirect(Constants.C_PAGINA_MENSAJE.ToString(), false);
        }

        public static string ObtieneKeyWebConfig(string istrKey)
        {
            return ConfigurationManager.AppSettings[istrKey];
        }

        /// <summary>
        /// Recorta un string largo agregando un espacio en blanco
        /// text:Cadena de texto que será recortado
        /// maxLenght: numero de caracteres
        /// Ejemplo: recortarString("hola",1) -- valor que regresará h o l a
        /// </summary>
        public static string recortarString(string text, int maxLength)
        {
            string sResult = "";
            int iContador = 1;
            int longitud = text.Length;
            int longitud_temp = 0;

            while (longitud_temp < longitud)
            {
                if (iContador == 1)
                {
                    sResult += text.Substring(0, maxLength) + " ";
                    longitud_temp = longitud_temp + maxLength;
                    iContador++;
                }
                else
                {
                    if ((longitud_temp + maxLength) <= longitud)
                    {
                        sResult += text.Substring(longitud_temp, maxLength) + " ";
                        longitud_temp = longitud_temp + maxLength;
                    }
                    else
                    {
                        sResult += text.Substring(longitud_temp, (longitud - longitud_temp));
                        longitud_temp = longitud_temp + maxLength;
                    }
                }
            }

            return sResult;
        }

        public static string EncryptPsw(string txtPassword)
        {
            byte[] passBytes = System.Text.Encoding.Unicode.GetBytes(txtPassword);
            string encryptPassword = Convert.ToBase64String(passBytes);
            return encryptPassword;
        }

        public static string DecryptPsw(string encryptedPassword)
        {
            byte[] passByteData = Convert.FromBase64String(encryptedPassword);
            string originalPassword = System.Text.Encoding.Unicode.GetString(passByteData);
            return originalPassword;
        }

        public static bool ValidaCorreo(string sCorreo)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(sCorreo,
                @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        //ACRUZ - SEGURIDAD CFDI MENUS - Se genera nueva funciones para el uso de la seguridad en CFDI. Mensaje de Error y validar acceso a cualquier pagina del CFDI
        public static void ManejaError(HttpResponse oResponse, string pPaginaRetorno)
        {
            Sessions.MensajeError = Constants.C_MENSAJE_ERROR_SEGTRANSACCION.ToString();
            if (String.IsNullOrEmpty(pPaginaRetorno))
            {
                Sessions.URLRetorno = Constants.C_PAGINA_DEFAULT.ToString();
            }
            else
            {
                Sessions.URLRetorno = pPaginaRetorno;
            }

            oResponse.Redirect(Constants.C_PAGINA_ERROR.ToString(), false);
        }

        public static void WriteInFileLog(string message, string fileName, string sActivaLog)
        {
            try
            {
                if (sActivaLog == "1")
                {
                    string rootPath = AppDomain.CurrentDomain.BaseDirectory;
                    using (FileStream fs = new FileStream(rootPath + "archivos\\" + fileName, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            //sw.WriteLine(DateTime.Now.ToString() + Environment.NewLine);
                            sw.WriteLine(message + " " + DateTime.Now.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }

    public static class Sessions
    {
        public static string MensajeError
        {
            get
            {
                string salida = "";
                if (HttpContext.Current.Session["MensajeError"] != null)
                {
                    salida = ((string)HttpContext.Current.Session["MensajeError"]);
                }
                return salida;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("MensajeError");
                }
                else
                {
                    HttpContext.Current.Session["MensajeError"] = value;
                }
            }
        }

        public static string StackTrace
        {
            get
            {
                string salida = "";
                if (HttpContext.Current.Session["StackTrace"] != null)
                {
                    salida = ((string)HttpContext.Current.Session["StackTrace"]);
                }
                return salida;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("StackTrace");
                }
                else
                {
                    HttpContext.Current.Session["StackTrace"] = value;
                }
            }
        }

        public static string URLRetorno
        {
            get
            {
                string salida = "";
                if (HttpContext.Current.Session["URLRetorno"] != null)
                {
                    salida = ((string)HttpContext.Current.Session["URLRetorno"]);
                }
                return salida;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("URLRetorno");
                }
                else
                {
                    HttpContext.Current.Session["URLRetorno"] = value;
                }
            }
        }

        public static string URLAplicacion
        {
            get
            {
                string salida = "";
                if (HttpContext.Current.Session["URLAplicacion"] != null)
                {
                    salida = ((string)HttpContext.Current.Session["URLAplicacion"]);
                }
                return salida;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("URLAplicacion");
                }
                else
                {
                    HttpContext.Current.Session["URLAplicacion"] = value;
                }
            }
        }

        public static string TraceError
        {
            get
            {
                string salida = "";
                if (HttpContext.Current.Session["TraceError"] != null)
                {
                    salida = ((string)HttpContext.Current.Session["TraceError"]);
                }
                return salida;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("TraceError");
                }
                else
                {
                    HttpContext.Current.Session["TraceError"] = value;
                }
            }
        }

        public static string Mensaje
        {
            get
            {
                string salida = "";
                if (HttpContext.Current.Session["Mensaje"] != null)
                {
                    salida = ((string)HttpContext.Current.Session["Mensaje"]);
                }
                return salida;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("Mensaje");
                }
                else
                {
                    HttpContext.Current.Session["Mensaje"] = value;
                }
            }
        }

        public static Entity.Usuario InfoUsuario
        {
            get
            {
                Entity.Usuario salida = null;
                if (HttpContext.Current.Session["InfoUsuario"] != null)
                {
                    salida = ((Entity.Usuario)HttpContext.Current.Session["InfoUsuario"]);
                }
                return salida;
            }
            set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("InfoUsuario");
                }
                else
                {
                    HttpContext.Current.Session["InfoUsuario"] = value;
                }
            }
        }

        //ACRUZ - SEGURIDAD CFDI MENUS - Se genera una variable de session para obtener el ROL
        public static List<Entity.TransaccionUsuario> SessionRol
        {
            get
            {
                if (HttpContext.Current.Session["SessionRol"] == null)
                {
                    HttpContext.Current.Session["SessionRol"] = new List<Entity.TransaccionUsuario>();
                }
                return HttpContext.Current.Session["SessionRol"] as List<Entity.TransaccionUsuario>;
            }
            set { HttpContext.Current.Session["SessionRol"] = value; }
        }

        //ACRUZ - SEGURIDAD CFDI MENUS - Se genera una variable de session para obtener el ROL
    }

    public static class Constants
    {
        //Constantes con los Nombres de Páginas
        public const string C_PAGINA_DEFAULT = "~/Login.aspx";
        public const string C_PAGINA_ERROR = "~/_error.aspx";
        public const string C_PAGINA_MENSAJE = "~/_mensaje.aspx";
        //Mensajes
        public const string C_MENSAJE_DEFAULT = "La acción indicada fue ejecutada correctamente.";

        public const string C_MENSAJE_ERROR_NO_LOGIN =
            "Ud. no ha iniciado sesión correctamente o se han permido la sesión que tenía, vuelva a ingresar.";

        public const string C_MENSAJE_ERROR_TIMEOUT = "Su sesion ha caducado, debe volver a ingresar al sistema.";

        public const string C_ERROR =
            "Ha ocurrido un error inesperado, por favor intente nuevamente y si persiste el error, contacte con el Administrador del Sistema.";

        public const string C_MENSAJE_ERROR_SEGTRANSACCION =
            "El usuario no tiene permisos para accesar a la pagina solicitada.";
    }

    public class Emailing
    {
        public int SmtpPort { get; set; }
        public string SmtpServer { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool EnableSsl { get; set; }
        public string EmailFrom { get; set; }

        /// <summary>    
        /// Envía los correos   
        /// </summary>    
        /// <param name="toEmailAddresses">';' seperated To email addresses.</param>    
        /// <param name="fromEmailAddress">From email address.</param>    
        /// <param name="subject">The subject.</param>    
        /// <param name="body">The body.</param>    
        /// <param name="mailAttachments">';' seperated mail attachments.</param>    
        /// <param name="isBodyHTML">is body HTML.</param>   
        public void SendMail(string toEmailAddresses, string subject,
            string body, string mailAttachments, bool isBodyHTML, string sConCopia)
        {
            char[] splitter = { ';' };
            string[] attachments = mailAttachments.Split(splitter);

            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(EmailFrom);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = isBodyHTML;
                if (sConCopia != "")
                {
                    mailMessage.CC.Add(new MailAddress(sConCopia));
                }

                //Adding Multiple To Addresses
                string[] mailAddresses = toEmailAddresses.Split(splitter);
                foreach (string mailAddress in mailAddresses)
                {
                    if (mailAddress.Length != 0)
                    {
                        mailMessage.To.Add(new MailAddress(mailAddress));
                    }
                }

                //Adding Multiple Attachments
                foreach (string attachment in attachments)
                {
                    if (attachment.Length != 0)
                    {
                        Attachment attachFile = new Attachment(attachment);
                        mailMessage.Attachments.Add(attachFile);
                    }
                }

                SmtpClient smtpClient = new SmtpClient();
                try
                {
                    smtpClient.Host = SmtpServer;
                    smtpClient.EnableSsl = EnableSsl;
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = UserName;
                    NetworkCred.Password = Password;
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = NetworkCred;
                    smtpClient.Port = SmtpPort;
                    smtpClient.Send(mailMessage);

                }
                catch (Exception ex)
                {
                    smtpClient = null;
                    throw ex;
                }
            }
            try
            {
                foreach (string attachment in attachments)
                {
                    if (File.Exists(attachment))
                        File.Delete(attachment);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>    
        /// Envía los correos en thread.    
        /// </summary>    
        /// <param name="toEmailAddresses">';' seperated To email addresses.</param>    
        /// <param name="fromEmailAddress">From email address.</param>    
        /// <param name="subject">The subject.</param>    
        /// <param name="body">The body.</param>    
        /// <param name="mailAttachments">';' seperated mail attachments.</param>    
        /// <param name="isBodyHTML">is body HTML.</param>    
        //public static void SendMailOnThread(string toEmailAddresses, string subject,
        //                                    string body, string mailAttachments, bool isBodyHTML,Entity.CFDIRecibo eRecibo, string sConCopia)
        //{
        //    Thread mailThread;
        //    mailThread = new System.Threading.Thread(delegate()
        //    {
        //        SendMail(toEmailAddresses, subject, body, mailAttachments, isBodyHTML,eRecibo,sConCopia);
        //    });
        //    mailThread.IsBackground = true;
        //    mailThread.Start();
        //}

    }

}
