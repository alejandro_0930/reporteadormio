﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Reporteador.Data;

namespace Reporteador.Logic
{
    public static class Reportes
    {

        public static DataSet RptTotalesCFDI(int iAnio, int iCompania, int iNomina, int iClave, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            DataSet ds = new DataSet();
            DATReportes dReportes = new DATReportes();
            ds = dReportes.RptTotalesCFDI(iAnio, iCompania, iNomina, iClave, dFechaDesde, dFechaHasta, sFiltroPeriodo);
            return ds;
        }

        public static DataSet RptTotalesCiaCFDI(int iAnio, int iCompania, int iNomina, int iClave, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            DataSet ds = new DataSet();
            DATReportes dReportes = new DATReportes();
            ds = dReportes.RptTotalesCiaCFDI(iAnio, iCompania, iNomina, iClave, dFechaDesde, dFechaHasta, sFiltroPeriodo);
            return ds;
        }

        public static DataSet RptDetalleErroresCFDI(int iAnio, int iCompania, int iNomina, int iClave, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            DataSet ds = new DataSet();
            DATReportes dReportes = new DATReportes();
            ds = dReportes.RptDetalleErroresCFDI(iAnio, iCompania, iNomina, iClave, dFechaDesde, dFechaHasta, sFiltroPeriodo);
            return ds;
        }

        public static DataSet RptDetalleCancelaCFDI(int iAnio, int iCompania, int iNomina, int iClave, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            DataSet ds = new DataSet();
            DATReportes dReportes = new DATReportes();
            ds = dReportes.RptDetalleCancelaCFDI(iAnio, iCompania, iNomina, iClave, dFechaDesde, dFechaHasta, sFiltroPeriodo);
            return ds;
        }

        public static DataSet RptDetalleValidadosCFDI(int iAnio, int iCompania, int iNomina, int iClave, string sStatus, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            DataSet ds = new DataSet();
            DATReportes dReportes = new DATReportes();
            ds = dReportes.RptDetalleValidadosCFDI(iAnio, iCompania, iNomina, iClave, sStatus, dFechaDesde, dFechaHasta, sFiltroPeriodo);
            return ds;
        }

        public static DataSet RptDetalleAcumuladosCFDI(int iAnio, int iCompania, int iNomina, int iClave, string sStatus, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            DataSet ds = new DataSet();
            DATReportes dReportes = new DATReportes();
            ds = dReportes.RptDetalleAcumuladosCFDI(iAnio, iCompania, iNomina, iClave, sStatus, dFechaDesde, dFechaHasta, sFiltroPeriodo);
            return ds;
        }

        public static DataSet RptDetalleRecuperacionesCFDI(int iAnio, int iCompania, int iNomina, int iClave, string sStatus, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            DataSet ds = new DataSet();
            DATReportes dReportes = new DATReportes();
            ds = dReportes.RptDetalleRecuperacionesCFDI(iAnio, iCompania, iNomina, iClave, sStatus, dFechaDesde, dFechaHasta, sFiltroPeriodo);
            return ds;
        }

        public static DataSet RptDetalleExpatriadosCFDI(int iAnio, int iCompania, int iNomina, int iClave, string sStatus, DateTime dFechaDesde, DateTime dFechaHasta, string sFiltroPeriodo)
        {
            DataSet ds = new DataSet();
            DATReportes dReportes = new DATReportes();
            ds = dReportes.RptDetalleExpatriadosCFDI(iAnio, iCompania, iNomina, iClave, sStatus, dFechaDesde, dFechaHasta, sFiltroPeriodo);
            return ds;
        }
    }
}
