﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Data;
using Reporteador.Logic;

namespace Reporteador.Logic
{
    public static class ClavesMovimiento
    {
        public static string CadenaConexion { get; set; }
        /// <summary>
        /// Obtiene Todas las claves de movimiento
        /// </summary>  
        public static List<Entity.ClavesMovimiento> ObtenClavesMovimiento()
        {
            List<Entity.ClavesMovimiento> lstClavesMovimientos;
            DATClavesMovimiento dClavesMovimiento = new DATClavesMovimiento();
            lstClavesMovimientos = dClavesMovimiento.ObtenClavesMovimiento();
            return lstClavesMovimientos;
        }

    }
}
