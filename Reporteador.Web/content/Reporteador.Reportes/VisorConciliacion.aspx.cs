﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Reporteador.Logic;
using ClosedXML.Excel;
using System.Threading;

namespace Reporteador.Web.content.Reporteador.Reportes
{
    public partial class VisorConciliacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                try
                {
                    txtNombreArchivo.Text = "Autos y Excesos";
                    cargaComboCompania(Sessions.InfoUsuario.UsuarioAcceso);
                    cargaComboClavesMovimiento();
                    cargaComboDips();
                    //Logic.Empleado.EliminaEmpleadoSeleccion(Sessions.InfoUsuario.UsuarioAcceso);
                    //Logic.Estructura.EliminaNivelMasivo(Sessions.InfoUsuario.UsuarioAcceso);
                }
                catch (Exception ex)
                {
                    Utilities.ManejaError(ex, Response, "~/Default.aspx");
                }
            }
        }

        protected void ddlCompania_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ReportViewerTotales.Reset();
            if (ddlCompania.SelectedValue != "0")
            {
                //Obtenemos las nominas de la compania seleccionada
                try
                {
                    limpiaCombos(ddlCompania.ID);
                    cargaComboNomina(Convert.ToInt32(ddlCompania.SelectedValue));
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Key", "alert('" + ex.Message + "')", true);
                    //MBoxx.ShowError("Error al intentar cargar los combos, Error: "+ EX.Message.ToString(), "Error", 150, 300);
                }
            }
            else
            {
                limpiaCombos(ddlCompania.ID);
            }
        }

        private void cargaComboCompania(string sUsuarioAcceso)
        {
            //Cargamos Companias
            //Logic.Compania lCompania = new Logic.Compania();
            List<Entity.Compania> lstCompanias;

            lstCompanias = Logic.Compania.ObtenCompania(Sessions.InfoUsuario.UsuarioAcceso);
            ddlCompania.DataSource = lstCompanias;
            ddlCompania.DataBind();

            ddlCompania.Items.Insert(0, new ListItem("", "0"));
            ddlCompania.SelectedIndex = 0;

        }

        private void cargaComboNomina(int iCompania)
        {
            //Logic.Nomina lNomina = new Nomina();
            List<Entity.Nomina> lstNominas;

            lstNominas = Logic.Nomina.ObtenNomina(iCompania, Sessions.InfoUsuario.UsuarioAcceso);
            ddlNomina.DataSource = lstNominas;
            ddlNomina.DataBind();

            ddlNomina.Items.Insert(0, new ListItem("", "0"));
            ddlNomina.SelectedIndex = 0;
        }

        private void cargaComboClavesMovimiento()
        {
            //Logic.ClavesMovimiento lClavesMovimiento = new Logic.ClavesMovimiento();
            List<Entity.ClavesMovimiento> lstClavesMovimientos;

            lstClavesMovimientos = Logic.ClavesMovimiento.ObtenClavesMovimiento();
            ddlClaveMovi.DataSource = lstClavesMovimientos;
            ddlClaveMovi.DataBind();

            ddlClaveMovi.Items.Insert(0, new ListItem("Todas", "0"));
            ddlClaveMovi.SelectedIndex = 0;
        }

        private void cargaComboDips()
        {
            //Logic.ClavesMovimiento lClavesMovimiento = new Logic.ClavesMovimiento();
            List<Entity.Dips> lstDips;

            lstDips = Logic.Dips.ObtenDips();
            ddlDip.DataSource = lstDips;
            ddlDip.DataBind();

            ddlDip.Items.Insert(0, new ListItem("", "0"));
            ddlDip.SelectedIndex = 0;
        }

        private void limpiaCombos(string sID)
        {
            switch (sID)
            {
                case "ddlCompania":
                    ddlNomina.Items.Clear();
                    break;
                case "ddlNomina":
                    break;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            string savePath = uplTheFile.FileName.ToString();
            //string filePath = "C:\\Juan Avalos\\flex\\Software Projects\\";
            string filePath = Path.GetTempPath();

            //if (Page.IsValid)
            //{

                if (uplTheFile.HasFile)   //Upload file here
                {
                    string fileExt = System.IO.Path.GetExtension(uplTheFile.FileName);  //Get extension

                    if (fileExt == ".csv" || fileExt == ".txt")   //check to see if its a .csv file
                    {
                        uplTheFile.SaveAs(Path.Combine(Path.GetTempPath(), savePath));

                        try
                        {

                            // Read File
                            SplitandReadFile(Path.GetTempPath() + savePath);

                            // Process to compare information
                            if (ChkBoxAuxBaan.Checked == true)
                                AuxiliarBaan(Path.GetTempPath());

                            ProcesosAcumulados(Path.GetTempPath());
                            ProcesosFiniquitos(Path.GetTempPath());
                            ProcesosIncapacidades(Path.GetTempPath());
                            ConciliationProcess(Path.GetTempPath());

                            //ScriptManager.RegisterStartupScript(this, GetType(), "Key", "alert('Pruebas')", true);
                        }

                        catch (Exception ee)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Key", "alert('" + ee.Message + "')", true);
                        }
                        finally
                        {
                        //ScriptManager.RegisterStartupScript(this, GetType(), "Key", "alert('Información conciliada con éxito')", true);
                        
                            Response.ContentType = "Application/vnd.ms-excel";
                            Response.AppendHeader("content-disposition", "attachment; filename=" + txtNombreArchivo.Text + ".xlsx");
                            Response.TransmitFile(filePath + txtNombreArchivo.Text + ".xlsx");
                            Response.End();

                            // Eliminamos el archivo temporal
                            if ((System.IO.File.Exists(filePath + txtNombreArchivo.Text + ".xlsx")))
                            {
                                System.IO.File.Delete(filePath + txtNombreArchivo.Text + ".xlsx");
                            }

                    }
                }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Key", "alert('Solo se permiten archivos csv')", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Key", "alert('No se ha especificado un archivo de Baan ')", true);
                }
            //}
        }

        private void SplitandReadFile(string strPath)
        {
            string strProcess = "";
            string strPeriod = "";
            string strDocument = "";
            string strNumber = "";
            string strDate = "";
            string strTaxPeriod = "";
            string strReportingPeriod = "";
            string strReference = "";
            string strCur = "";
            string strAmountFC = "";
            string strAmountDebit = "";
            string strAmountCredit = "";
            string strUsuario = Sessions.InfoUsuario.UsuarioAcceso;
            string strAnio = ddlAnio.Text;

            var lines = File.ReadAllLines(strPath);
            int dataRowStart = 2;

            //Random rnd = new Random();
            //intUsuario = rnd.Next(1, 10000);


            // Borrado del Auxiliar de Baan
            Logic.AutosExcesos.BorraAutosBaan(strUsuario);

            for (int i = dataRowStart; i < lines.Length - 1; i++)
            {
                string line = lines[i];
                string[] values = line.Split('|');

                
                strPeriod = values[0].Trim();

                if (strPeriod.Contains("Ledger"))
                {
                    if (values[3].Trim().Contains("CAR INSURANCE"))
                        strProcess = "Autos";
                    else
                        strProcess = "Excesos";
                }

                if (strPeriod.Contains(strAnio) || strPeriod.Contains((Int32.Parse(strAnio) + 1).ToString()))
                {
                    if (strProcess == RdoBtnLstAutos.SelectedValue)
                    {
                        strDocument = values[1].Trim();
                        strNumber = values[2].Trim();
                        strDate = values[3].Trim();
                        strTaxPeriod = values[4].Trim();
                        strReportingPeriod = values[5].Trim();
                        strReference = values[6].Trim();
                        strCur = values[7].Trim();
                        strAmountFC = values[8].Trim();
                        strAmountDebit = values[9].Trim();
                        strAmountCredit = values[10].Trim().Replace(",", "");
                        //strAmountCredit = float.Parse(values[10].Trim()).ToString("0.00");

                        Logic.AutosExcesos.InsertaAutosBaan(strUsuario, strProcess, strPeriod, strDocument, strNumber, strDate, strTaxPeriod, strReportingPeriod, strReference, strCur, strAmountFC, strAmountDebit, strAmountCredit);
                    }
                }
            }
        }

        private void AuxiliarBaan(string folderPath)
        {
            string savePath = uplTheFile.FileName.ToString();
            //string folderPath = "C:\\Juan Avalos\\flex\\Software Projects\\";

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            string strText = File.ReadAllText(Path.Combine(Path.GetTempPath(), savePath));

            var wb = new XLWorkbook();
            {
                var ws = wb.Worksheets.Add("Auxiliar Baan");
                ws.Cell("A1").Value = strText;
                

                wb.SaveAs(folderPath + txtNombreArchivo.Text + ".xlsx");
            }
        }

        private void ProcesosAcumulados(string folderPath)
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            //string query = "select compania, razon_social from companias ";
            string strQuery = "Reporteador_conciliacion_acumulados";

            int intAnio = Int32.Parse(ddlAnio.Text);
            int intNomina = 0;
            int intClave = 0;
            string strDip = ddlDip.Text;

            //string strPerQuin = "121, 122";
            //string strPerSem = "262, 263, 264, 265, 266";

            int intCompania = Convert.ToInt32(ddlCompania.SelectedValue);
            if (String.IsNullOrEmpty(ddlNomina.SelectedValue))
                intNomina = 0;
            else
                intNomina = Convert.ToInt32(ddlNomina.SelectedValue);

            if (String.IsNullOrEmpty(ddlClaveMovi.SelectedValue))
                intClave = 0;
            else
                intClave = Convert.ToInt32(ddlClaveMovi.SelectedValue);

            

            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(strQuery, conn);
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add("@anio", System.Data.SqlDbType.Int).Value = intAnio;
            cmd.Parameters.Add("@compania", System.Data.SqlDbType.Int).Value = intCompania;
            cmd.Parameters.Add("@nomina", System.Data.SqlDbType.Int).Value = intNomina;
            cmd.Parameters.Add("@clave", System.Data.SqlDbType.Int).Value = intClave;
            cmd.Parameters.Add("@dip", System.Data.SqlDbType.VarChar).Value = strDip;
            //cmd.Parameters.Add("@periodoquin", System.Data.SqlDbType.VarChar).Value = strPerQuin;
            //cmd.Parameters.Add("@periodosem", System.Data.SqlDbType.VarChar).Value = strPerSem;
            cmd.Parameters.Add("@fecha_desde", System.Data.SqlDbType.DateTime).Value = txtFechaDesde.Text;
            cmd.Parameters.Add("@fecha_hasta", System.Data.SqlDbType.DateTime).Value = txtFechaHasta.Text;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dt);
            conn.Close();
            da.Dispose();

            //Exporting to Excel
            //string folderPath = "C:\\Juan Avalos\\flex\\Software Projects\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook(folderPath + txtNombreArchivo.Text + ".xlsx"))
            {
                var ws = wb.Worksheets.Add(dt, strDip + " Nominas");
                if (dt.Rows.Count != 0)
                {
                    ws.Range("K" + (dt.Rows.Count + 2).ToString()).FormulaA1 = "=SUM(K2:K" + (dt.Rows.Count + 1).ToString() + ")";
                    ws.Cell(dt.Rows.Count + 2, 11).Style.NumberFormat.Format = "#,##0.00";
                    ws.Columns(1, 23).AdjustToContents();
                    Session["Acum_Row"] = (dt.Rows.Count + 2).ToString();
                }
                wb.Save();
                
                dt.Dispose();
            }
        }

        private void ProcesosFiniquitos(string folderPath)
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            //string strQuery = "select compania, razon_social from companias ";
            string strQuery = "Reporteador_conciliacion_finiquitos";

            int intAnio = Int32.Parse(ddlAnio.Text);
            int intNomina = 0;
            int intClave = 0;
            //string strDip = "D069";
            string strDip = ddlDip.Text;
            //string strPerQuin = "121, 122";
            //string strPerSem = "262, 263, 264, 265, 266";

            int intCompania = Convert.ToInt32(ddlCompania.SelectedValue);
            if (String.IsNullOrEmpty(ddlNomina.SelectedValue))
                intNomina = 0;
            else
                intNomina = Convert.ToInt32(ddlNomina.SelectedValue);

            if (String.IsNullOrEmpty(ddlClaveMovi.SelectedValue))
                intClave = 0;
            else
                intClave = Convert.ToInt32(ddlClaveMovi.SelectedValue);

            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(strQuery, conn);
            cmd.CommandTimeout = 0;
            cmd.Parameters.Add("@anio", System.Data.SqlDbType.Int).Value = intAnio;
            cmd.Parameters.Add("@compania", System.Data.SqlDbType.Int).Value = intCompania;
            cmd.Parameters.Add("@nomina", System.Data.SqlDbType.Int).Value = intNomina;
            cmd.Parameters.Add("@clave", System.Data.SqlDbType.Int).Value = intClave;
            cmd.Parameters.Add("@dip", System.Data.SqlDbType.VarChar).Value = strDip;
            //cmd.Parameters.Add("@periodoquin", System.Data.SqlDbType.VarChar).Value = strPerQuin;
            //cmd.Parameters.Add("@periodosem", System.Data.SqlDbType.VarChar).Value = strPerSem;
            cmd.Parameters.Add("@fecha_desde", System.Data.SqlDbType.DateTime).Value = txtFechaDesde.Text;
            cmd.Parameters.Add("@fecha_hasta", System.Data.SqlDbType.DateTime).Value = txtFechaHasta.Text;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dt);
            conn.Close();
            da.Dispose();

            //Exporting to Excel
            //string folderPath = "C:\\Juan Avalos\\flex\\Software Projects\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook(folderPath + txtNombreArchivo.Text + ".xlsx"))
            {
                var ws = wb.Worksheets.Add(dt, "Finiquitos");
                if (dt.Rows.Count != 0)
                { 
                    ws.Range("E" + (dt.Rows.Count + 2).ToString()).FormulaA1 = "=SUM(E2:E" + (dt.Rows.Count + 1).ToString() + ")";
                    ws.Cell(dt.Rows.Count + 2, 5).Style.NumberFormat.Format = "#,##0.00";
                    ws.Columns(1, 7).AdjustToContents();
                    Session["Fin_Row"] = (dt.Rows.Count + 2).ToString();
                }
                wb.Save();

                dt.Dispose();
            }
        }

        private void ProcesosIncapacidades(string folderPath)
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            //string strQuery = "select compania, razon_social from companias ";
            string strQuery = "Reporteador_conciliacion_incapacidades";

            int intAnio = Int32.Parse(ddlAnio.Text);
            int intNomina = 0;
            int intClave = 0;
            //string strDip = "D069";
            string strDip = ddlDip.Text;

            //string strPerQuin = "121, 122";
            //string strPerSem = "262, 263, 264, 265, 266";

            int intCompania = Convert.ToInt32(ddlCompania.SelectedValue);
            if (String.IsNullOrEmpty(ddlNomina.SelectedValue))
                intNomina = 0;
            else
                intNomina = Convert.ToInt32(ddlNomina.SelectedValue);

            if (String.IsNullOrEmpty(ddlClaveMovi.SelectedValue))
                intClave = 0;
            else
                intClave = Convert.ToInt32(ddlClaveMovi.SelectedValue);

            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(strQuery, conn);
            cmd.CommandTimeout = 0;
            cmd.Parameters.Add("@anio", System.Data.SqlDbType.Int).Value = intAnio;
            cmd.Parameters.Add("@compania", System.Data.SqlDbType.Int).Value = intCompania;
            cmd.Parameters.Add("@nomina", System.Data.SqlDbType.Int).Value = intNomina;
            cmd.Parameters.Add("@clave", System.Data.SqlDbType.Int).Value = intClave;
            cmd.Parameters.Add("@dip", System.Data.SqlDbType.VarChar).Value = strDip;
            //cmd.Parameters.Add("@periodoquin", System.Data.SqlDbType.VarChar).Value = strPerQuin;
            //cmd.Parameters.Add("@periodosem", System.Data.SqlDbType.VarChar).Value = strPerSem;
            cmd.Parameters.Add("@fecha_desde", System.Data.SqlDbType.DateTime).Value = txtFechaDesde.Text;
            cmd.Parameters.Add("@fecha_hasta", System.Data.SqlDbType.DateTime).Value = txtFechaHasta.Text;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dt);
            conn.Close();
            da.Dispose();

            //Exporting to Excel
            //string folderPath = "C:\\Juan Avalos\\flex\\Software Projects\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook(folderPath + txtNombreArchivo.Text + ".xlsx"))
            {
                var ws = wb.Worksheets.Add(dt, "Incapacidades");
                if (dt.Rows.Count != 0)
                { 
                    ws.Range("C" + (dt.Rows.Count + 2).ToString()).FormulaA1 = "=SUM(C2:C" + (dt.Rows.Count + 1).ToString() + ")";
                    ws.Cell(dt.Rows.Count + 2, 3).Style.NumberFormat.Format = "#,##0.00";
                    ws.Columns(1, 3).AdjustToContents();
                    Session["Inc_Row"] = (dt.Rows.Count + 2).ToString();
                }
                wb.Save();

                dt.Dispose();
            }
        }

        private void ConciliationProcess(string folderPath)
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            //string strQuery = "select compania, razon_social from companias ";
            string strQuery = "Reporteador_Conciliacion_Autos_Excesos";
            string strUsuario = Sessions.InfoUsuario.UsuarioAcceso;

            int intAnio = Int32.Parse(ddlAnio.Text);
            int intNomina = 0;
            int intClave = 0;
            //string strDip = "D069";
            string strDip = ddlDip.Text;

            //string strPerQuin = "121, 122";
            //string strPerSem = "262, 263, 264, 265, 266";

            int intCompania = Convert.ToInt32(ddlCompania.SelectedValue);
            if (String.IsNullOrEmpty(ddlNomina.SelectedValue))
                intNomina = 0;
            else
                intNomina = Convert.ToInt32(ddlNomina.SelectedValue);

            if (String.IsNullOrEmpty(ddlClaveMovi.SelectedValue))
                intClave = 0;
            else
                intClave = Convert.ToInt32(ddlClaveMovi.SelectedValue);

            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(strQuery, conn);
            cmd.CommandTimeout = 0;
            cmd.Parameters.Add("@usuario", System.Data.SqlDbType.VarChar).Value = strUsuario;
            cmd.Parameters.Add("@compania", System.Data.SqlDbType.Int).Value = intCompania;
            cmd.Parameters.Add("@nomina", System.Data.SqlDbType.Int).Value = intNomina;
            cmd.Parameters.Add("@anio", System.Data.SqlDbType.Int).Value = intAnio;
            //cmd.Parameters.Add("@clave", System.Data.SqlDbType.Int).Value = intClave;
            //cmd.Parameters.Add("@dip", System.Data.SqlDbType.VarChar).Value = strDip;
            ////cmd.Parameters.Add("@periodoquin", System.Data.SqlDbType.VarChar).Value = strPerQuin;
            ////cmd.Parameters.Add("@periodosem", System.Data.SqlDbType.VarChar).Value = strPerSem;
            cmd.Parameters.Add("@fecha_desde", System.Data.SqlDbType.DateTime).Value = txtFechaDesde.Text;
            cmd.Parameters.Add("@fecha_hasta", System.Data.SqlDbType.DateTime).Value = txtFechaHasta.Text;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dt);
            conn.Close();
            da.Dispose();

            //Exporting to Excel
            //string folderPath = "C:\\Juan Avalos\\flex\\Software Projects\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            using (XLWorkbook wb = new XLWorkbook(folderPath + txtNombreArchivo.Text + ".xlsx"))
            {
                var ws = wb.Worksheets.Add("Resumen");

                // Titles
                ws.Cell(1, 1).Value = ddlCompania.SelectedItem.Text.ToUpper();
                if (RdoBtnLstAutos.Text == "Autos")
                    ws.Cell(2, 1).Value = "CONCILIACION DE AUTOS " + ddlMes.SelectedItem.Text.ToUpper() + " " + ddlAnio.Text;
                else
                    ws.Cell(2, 1).Value = "CONCILIACION DE EXCESOS " + ddlMes.SelectedItem.Text.ToUpper() + " " + ddlAnio.Text;

                ws.Cell(3, 1).Value = ddlNomina.SelectedItem.Text.ToUpper(); //"INDIRECTOS";

                // Totals
                ws.Cell(5, 3).Value = "ESLABON";
                ws.Range("C5:D5").Merge();
                ws.Cell(5, 3).Style.Fill.BackgroundColor = XLColor.LightGray;
                ws.Cell(6, 3).Value = "NOMINA";
                var SourceSheet = wb.Worksheet(strDip + " Nominas");
                ws.Cell(6, 4).Value = SourceSheet.Cell(Int32.Parse(Session["Acum_Row"].ToString()), 11).Value;
                ws.Cell(6, 4).Style.NumberFormat.Format = "#,##0.00";
                SourceSheet.Dispose();

                ws.Cell(7, 3).Value = "FINIQUITOS";
                var SourceSheet1 = wb.Worksheet("Finiquitos");
                ws.Cell(7, 4).Value = SourceSheet1.Cell(Int32.Parse(Session["Fin_Row"].ToString()), 5).Value;
                ws.Cell(7, 4).Style.NumberFormat.Format = "#,##0.00";
                SourceSheet1.Dispose();

                ws.Cell(8, 3).Value = "INCAPACIDADES";
                var SourceSheet2 = wb.Worksheet("Incapacidades");
                ws.Cell(8, 4).Value = SourceSheet2.Cell(Int32.Parse(Session["Inc_Row"].ToString()), 3).Value;
                ws.Cell(8, 4).Style.NumberFormat.Format = "#,##0.00";
                SourceSheet2.Dispose();

                ws.Cell(9, 3).Value = "REEMBOLSOS";
                ws.Cell(9, 4).Value = "0";
                ws.Cell(9, 4).Style.NumberFormat.Format = "#,##0.00";

                // Suma totales
                ws.Range("D11").FormulaA1 = "=SUM(D6:D10)";
                ws.Cell(11, 4).Style.NumberFormat.Format = "#,##0.00";

                ws.Columns(3, 4).AdjustToContents();

                //ws.Range("G" + (dt.Rows.Count + 2).ToString()).FormulaA1 = "=SUM(G2:G" + (dt.Rows.Count + 1).ToString() + ")";

                if (ChkBoxBaan.Checked == true)
                {
                    if (dt.Rows.Count != 0)
                    {
                        ws.Cell(5, 6).Value = "BAAN";
                        ws.Range("F5:G5").Merge();
                        ws.Cell(5, 6).Style.Fill.BackgroundColor = XLColor.LightGray;

                        // Nomina
                        ws.Cell(6, 6).Value = dt.Rows[0]["concepto"].ToString();
                        ws.Cell(6, 7).Value = dt.Rows[0]["total"].ToString();
                        ws.Cell(6, 7).Style.NumberFormat.Format = "#,##0.00";

                        // Finiquitos
                        ws.Cell(7, 6).Value = dt.Rows[1]["concepto"].ToString();
                        ws.Cell(7, 7).Value = dt.Rows[1]["total"].ToString();
                        ws.Cell(7, 7).Style.NumberFormat.Format = "#,##0.00";

                        ws.Range("G11").FormulaA1 = "=SUM(G6:G10)";
                        ws.Cell(11, 7).Style.NumberFormat.Format = "#,##0.00";

                        ws.Columns(6, 7).AdjustToContents();
                    }
                }
                wb.Save();

                dt.Dispose();
            }
        }
    }
}