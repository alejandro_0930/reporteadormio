﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reporteador.Master" AutoEventWireup="true" CodeBehind="VisorReporte.aspx.cs" 
    Inherits="Reporteador.Web.content.Reporteador.Reportes.VisorReporte" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="master_content" runat="server">
    <link href="../../css/normalize.css" rel="stylesheet" type="text/css" />
    <link id="cssGeneral" rel="stylesheet" type="text/css" />
    <link id="cssMenu" rel="stylesheet" type="text/css" />
    <link id="cssGrid" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            width: 120px;
            height: 23px;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            width: 84px;
            height: 23px;
        }
        .auto-style5 {
            height: 5px;
            width: 94px;
        }
        .auto-style6 {
            width: 94px;
        }
        .auto-style7 {
            width: 94px;
            height: 23px;
        }
        .auto-style8 {
            width: 84px;
        }
        .auto-style10 {
            height: 5px;
            width: 303px;
        }
        .auto-style12 {
            width: 303px;
            height: 23px;
        }
        .auto-style20 {
            height: 15px;
        }
        .auto-style21 {
            width: 94px;
            height: 15px;
        }
        .auto-style22 {
            width: 303px;
            height: 15px;
        }
        .auto-style23 {
            cursor: pointer;
            margin-left: 0;
        }
        .auto-style24 {
            width: 100%;
        }
        .auto-style26 {
            width: 96px;
        }
        .auto-style27 {
            height: 15px;
            width: 96px;
        }
        .auto-style30 {
            width: 126px;
        }
        .auto-style29 {
            height: 21px;
            width: 126px;
        }
        .auto-style25 {
            height: 21px;
        }
        </style>
    <asp:UpdatePanel ID="udpContent" runat="server" UpdateMode="Always">
        <ContentTemplate>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"
     AsyncPostBackTimeout="7200" EnableScriptGlobalization="True">
    </ajaxToolkit:ToolkitScriptManager>
    
      <div id="main"style="background: #fff;color: #bebebe;
	font-family: Tahoma, Arial, Helvetica, sans-serif;
	font-size: 11px;
	margin: 0;">
    <div class="full_w">
                <div class="h_title">
                </div>
                <h1>
                    <asp:HiddenField ID="hfIdReporte" runat="server" />
                    <asp:Label ID="lblNombreReporte" runat="server"></asp:Label>
                </h1>
                <div class="margen_container">                    
                         <table>
                <tr>
                    <td class="auto-style1" style="border-bottom-style: groove">
                        <strong>GENERALES</strong></td>
                    <td class="auto-style2" style="border-bottom-style: groove">
                        </td>
                    <td class="auto-style3" style="border-bottom-style: groove">
                        </td>
                    <td class="auto-style1" style="border-bottom-style: groove">
                        <strong>RECIBOS</strong></td>
                    <td class="auto-style2" style="border-bottom-style: groove">
                        &nbsp;</td>
                    <td class="auto-style7" style="border-bottom-style: groove">
                        </td>
                    <td class="auto-style7" style="border-bottom-style: groove">
                        <strong>PERIODOS</strong></td>
                    <td class="auto-style12" style="border-bottom-style: groove">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                    </td>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td colspan="2" style="vertical-align: top;">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                    <td class="auto-style12">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" rowspan="5" style="vertical-align: top">
                        <table class="auto-style24">
                            <tr>
                                <td class="auto-style1">
                        <span>Compañia:</span></td>
                                <td class="auto-style1">
                        <asp:DropDownList ID="ddlCompania" runat="server" CssClass="ddl_css" AutoPostBack="true"
                            Width="300" DataTextField="RazonSocial" DataValueField="IdCompania" OnSelectedIndexChanged="ddlCompania_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCompania" runat="server" ControlToValidate="ddlCompania"
                            Display="Dynamic" ValidationGroup="buscar" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style29"><span>Nómina:</span></td>
                                <td class="auto-style25">
                        <asp:DropDownList ID="ddlNomina" runat="server" CssClass="ddl_css" DataTextField="Descripcion"
                            Width="300" DataValueField="IdNomina" OnSelectedIndexChanged="ddlNomina_SelectedIndexChanged">
                        </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style30">
                        <span>Clave Movimiento:</span></td>
                                <td>
                    <asp:DropDownList ID="ddlClaveMovi" runat="server" CssClass="ddl_css" DataTextField="Descripcion"
                            Width="300" DataValueField="ClaveMovimiento" OnSelectedIndexChanged="ddlClaveMovi_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvClaveMovimiento" runat="server" ControlToValidate="ddlClaveMovi"
                            Display="Dynamic" ValidationGroup="buscar" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                        
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style30">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td class="auto-style3">
                    </td>
                    <td colspan="2" rowspan="5" style="vertical-align: top;">
                        <table class="auto-style24">
                            <tr>
                                <td style="vertical-align: top">Agrupación:</td>
                                <td style="vertical-align: top">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                <td>
                        <asp:RadioButtonList ID="RdoBtnLstAgrupacion" runat="server" Height="16px" Width="173px" AutoPostBack="True" OnSelectedIndexChanged="RdoBtnLstAgrupacion_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="TotalGeneral"> Total General</asp:ListItem>
                            <asp:ListItem Value="TotalCia"> Total por Compañía</asp:ListItem>
                            <asp:ListItem Value="Detalle"> Detalle</asp:ListItem>
                        </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style20"></td>
                                <td class="auto-style20">&nbsp;</td>
                                <td class="auto-style20"></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LblEstatus" runat="server" Enabled="False" Text="Estatus:"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>
                    <asp:DropDownList ID="ddlEstatus" runat="server" CssClass="ddl_css" Width="200" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="ddlEstatus_SelectedIndexChanged">
                            <asp:ListItem Text="Errores" Value="1"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Cancelados"></asp:ListItem>                            
                            <asp:ListItem Value="3">Validados</asp:ListItem>
                            <asp:ListItem Value="4">Por Validar</asp:ListItem>
                            <asp:ListItem Value="5">Acumulados (No timbrados)</asp:ListItem>
                            <asp:ListItem Value="6">Pendientes recuperar</asp:ListItem>
                            <asp:ListItem Value="7">Timbrados (Expatriados)</asp:ListItem>
                        </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="auto-style7">
                        </td>
                    <td colspan="2" rowspan="5" style="vertical-align: top">
                        <table class="auto-style24">
                            <tr>
                                <td class="auto-style26">
                                    <asp:RadioButton ID="RdoBtnAnio" runat="server" AutoPostBack="True" Checked="True" OnCheckedChanged="RdoBtnAnio_CheckedChanged" Text="  Año:" />
                                </td>
                                <td colspan="2">
                    <asp:DropDownList ID="ddlAnio" runat="server" CssClass="ddl_css" Width="93px" Height="16px" style="margin-left: 0">
                            <asp:ListItem Selected="True">2018</asp:ListItem>
                            <asp:ListItem>2017</asp:ListItem>                            
                            <asp:ListItem>2016</asp:ListItem>
                        </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style27"></td>
                                <td class="auto-style20" colspan="2"></td>
                            </tr>
                            <tr>
                                <td class="auto-style26">
                                    <asp:RadioButton ID="RdoBtnFechas" runat="server" AutoPostBack="True" OnCheckedChanged="RdoBtnFechas_CheckedChanged" Text="  Fechas:" />
                                </td>
                                <td>
                                
                                    Desde:</td>
                                <td>
                                
                                    <asp:TextBox ID="txtFechaDesde" runat="server" MaxLength="10" CssClass="auto-style23" Enabled="False"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaDesde"
                                        Format="dd/MM/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style26">&nbsp;</td>
                                <td>Hasta:</td>
                                <td><asp:TextBox ID="txtFechaHasta" runat="server" MaxLength="10" CssClass="auto-style23" Enabled="False"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="txtFechaHasta_CalendarExtender" runat="server" TargetControlID="txtFechaHasta"
                                        Format="dd/MM/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style8">
                    </td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style8">
                        &nbsp;</td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        
                    </td>
                    <td class="auto-style7">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td class="auto-style3">
                        
                        &nbsp;</td>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                    <td class="auto-style12">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style20">
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="buton btn_buscar"
                            Width="100" OnClick="btnBuscar_Click" ValidationGroup="buscar" />
                    </td>
                    <td colspan="2" class="auto-style20">
                        </td>
                    <td class="auto-style20"> </td>
                    <td class="auto-style20">
                        </td>
                    <td class="auto-style21">
                        </td>
                    <td class="auto-style21">
                        </td>
                    <td class="auto-style22">
                        </td>
                </tr>
                <tr>
                    <td style="height: 5px" colspan="5">
                        &nbsp;</td>
                    <td class="auto-style5">
                        &nbsp;</td>
                    <td class="auto-style5">
                        &nbsp;</td>
                    <td class="auto-style10">
                        &nbsp;</td>
                </tr>
                </table>
                         <br />
                <div class="h_title">
                </div>
                </div>
                      
            </div>
    <div>
        <rsweb:ReportViewer ID="ReportViewerTotales" runat="server" Width="469px"></rsweb:ReportViewer>
    </div>
    </div>
    
    <div>
    </div>

    </form>

    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

